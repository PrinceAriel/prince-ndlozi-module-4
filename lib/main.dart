import 'package:flutter/material.dart';
import 'package:module_04/profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo Profile',
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      theme: ThemeData(
          primarySwatch: Colors.amber,
          accentColor: Colors.amberAccent,
          backgroundColor: Colors.white70),
    );
  }
}
